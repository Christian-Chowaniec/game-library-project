import React from 'react';

const SearchBar = () => {
    return (
        <>
            <div className="search-box">
                <button className="btn-search-circle">
                    <div className="gg-search_icon-circle">
                        <i className="gg-search"></i>

                    </div>
                </button>
                {/*<input type="text"*/}
                {/*       className="input-search"*/}
                {/*       placeholder="Type to Search..."*/}
                {/*/>*/}
            </div>
        </>
    );
};

export default SearchBar;
